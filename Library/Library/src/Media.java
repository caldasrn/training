import java.time.Year;

public abstract class Media {
    private String name;
    private Year publishYear;
    private String type;

    public Media(String name, Year publishYear, String type) {
        this.name = name;
        this.publishYear = publishYear;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Year getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Year publishYear) {
        this.publishYear = publishYear;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
}