public class TestExceptions {
    public static void main(String[] args) {
        Account[] accountsArray = new Account[5];

        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Fingers", "Worf", "Troy", "Data"};
        
        try{
            for (int i = 0; i < accountsArray.length; i++) {
                accountsArray[i] = new CurrentAccount(names[i], amounts[i]);
                System.out.println("Account Details: " + accountsArray[i].getDetails());

                accountsArray[i].addInterest();
                System.out.println("The new Balance is: $" + String.format("10.2f", accountsArray[i].getBalance()));
            }
        }
        catch(DodgyNameException e){
            System.out.println("Exception " + e);
        }
    }
}