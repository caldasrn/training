public class Car {
  private String make;
  private String model;

  private static int speedLimit = 70;
   
  public Car(){}

  public Car(String md, String mk) {
      make = mk;
      model = md;
  }
  public String getModel() {
      return model;
  }
  public void setModel(String model) {
      this.model = model;
  }
  public String getMake() {
      return make;
  }
  public void setMake(String make) {
      this.make = make;
  }

  public static int getSpeedLimit() {
      return speedLimit;
  }

  public static void setSpeedLimit(int speedLimit) {
      Car.speedLimit = speedLimit;
  }
}