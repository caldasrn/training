public class MyFirstClass{
    public static void main(String[] args) {

        System.out.println("Hello from my first Java program!");
        
        String make = "Audi";
        String model = "Q3";
        double engineSize = 3.0;
        byte gear = 2;
        short speed = (short)(gear * 20);

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);
        System.out.println("The current speed is " + speed);

        if (engineSize > 1.3){
            System.out.println("The car is a prowerful car.");
        } else{
            System.out.println("The car is a weak car.");
        }

        for (int i = 1900, count = 0; i <= 2000; i++){
            if (i % 4 == 0){
                if (i % 100 == 0){
                    if (i % 400 == 0){
                        System.out.println(i + " is a leap year.");
                        count += 1;                        
                    }
                }else{
                    System.out.println(i + " is a leap year.");
                    count += 1;
                }
            }
            if (count == 5) {
                System.out.println("Finished");
                break;
            }
        }

        for (int i = 1900, count = 0; i <= 2000; i++){
            switch (i % 4){
                case 0 :
                    switch (i % 100){
                        case 0 :
                            switch (i % 400) {
                                case 0 : 
                                    System.out.println(i + " is a leap year.");
                                    count += 1;
                                    break;
                            }
                            break;
                        default:
                            System.out.println(i + " is a leap year.");
                            count += 1;
                            break;
                    }
                    break;
                default: break;
            }
            if (count == 5) {
                System.out.println("Finished");
                break;
            }
        }

        Car myCar = new Car();
        Car myNextCar = new Car("Model S", "Tesla");

        myCar.setMake(make);
        myCar.setModel(model);

        System.out.println("My car is the: " + myCar.getMake() + " " + myCar.getModel() + " and it is in gear " + gear + ".");
        System.out.println("My next car will be the: " + myNextCar.getMake() + " " + myNextCar.getModel() + ".");
    }
}