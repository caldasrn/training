public class TestInheritance {
    //Chapter 11: Inheritance and Abstraction
    public static void main(String[] args) {
        Account[] accounts = new Account[3];

        accounts[0] = new SavingsAccount("Rod1", 2);
        accounts[1] = new CurrentAccount("Rod2", 4);
        accounts[2] = new SavingsAccount("Rod3", 6);
        
        for(Account acc : accounts){
            acc.addInterest();
            System.out.println(acc.getName() + " $" + acc.getBalance());
        }
    }    
}