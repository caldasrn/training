public class TestAccount2 {
    //Chapter 8 More on Java Classes
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        for (int i = 0; i < arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new CurrentAccount("Rod" + i, 50.0);
        }
        Account.setInterestRate(.2);

        for (int i = 0; i < arrayOfAccounts.length; i++){
            System.out.println("Account Holder: " + arrayOfAccounts[i].getName());
            System.out.println("Account Balance: " + String.format("%10.2f", arrayOfAccounts[i].getBalance()) + "\n");

            arrayOfAccounts[i].addInterest();

            System.out.println("The new Account Balance: " + String.format("%10.2f", arrayOfAccounts[i].getBalance()) + "\n");
        }

        double amount = 80;

        arrayOfAccounts[2].withdraw(amount);
        arrayOfAccounts[4].withdraw();
    }    
}