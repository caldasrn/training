public class CurrentAccount extends Account{
    //Chapter 11: Inheritance and Abstraction

    public CurrentAccount(String name, double balance) throws DodgyNameException{
        super(name, balance);
    }
    
    @Override
    public void addInterest() {
        this.setBalance(this.getBalance() * 1.1);
    }
}