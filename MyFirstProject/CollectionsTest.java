import java.util.HashSet;

public class CollectionsTest {
    //Chapter 14: The Java Collections API
    public static void main(String[] args) {
        HashSet<Account> accounts = new HashSet<Account>();
        
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        
        Account currentAccount;
        for (int i = 0; i < 5; i++){
            currentAccount = new SavingsAccount();
            currentAccount.setName(names[i]);
            currentAccount.setBalance(amounts[i]);
            accounts.add(currentAccount);
        }
    }
}