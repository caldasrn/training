//Chapter 6: Introduction to Objects in Java
public class TestAccount{
    public static void main(String[] args) {
        Account myAccount = new SavingsAccount();
        myAccount.setName("Rod");
        myAccount.setBalance(100000.00);

        System.out.println("Account Holder: " + myAccount.getName());
        System.out.println("Account Balance: $" + String.format("%10.2f", myAccount.getBalance()));
        myAccount.addInterest();
        System.out.println("Account Holder: " + myAccount.getName());
        System.out.println("Account Balance: $" + String.format("%10.2f", myAccount.getBalance()));

        //Chapter 7: Working with Arrays
        Account[] arrayOfAccounts = new Account[5];
        double[] ammounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data",};

        for (int i = 0; i < 5; i++){

            arrayOfAccounts[i] = new SavingsAccount();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(ammounts[i]);

            System.out.println("Account Holder: " + arrayOfAccounts[i].getName());
            System.out.println("Account Balance: " + String.format("%10.2f", arrayOfAccounts[i].getBalance()) + "\n");

            arrayOfAccounts[i].addInterest();

            System.out.println("The new Account Balance: " + String.format("%10.2f", arrayOfAccounts[i].getBalance()) + "\n");
        }
    }
}