public interface Detailable {
    //Chapter 12: Interfaces
    public abstract String getDetails();
}