package com.conygre.training.demo.accounts.dao;

import java.util.ArrayList;
import java.util.List;

import com.conygre.training.demo.accounts.entity.Account;

import org.springframework.stereotype.Component;

@Component
public class AccountList {
    private List<Account> accountList;

    public AccountList(){
        this.accountList = new ArrayList<Account>();
    }

    public List<Account> findAll() {
        return this.accountList;
    }

    public void insert(Account account) {
        this.accountList.add(account);
    }
}