package com.conygre.training.demo.accounts.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

import com.conygre.training.demo.accounts.entity.Account;

public interface AccountMongoRepository extends MongoRepository<Account, ObjectId> {

	public List<Account> findByName(String name);
}