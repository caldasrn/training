package com.conygre.training.demo.accounts.service;

import com.conygre.training.demo.accounts.entity.Account;
import com.conygre.training.demo.accounts.dao.AccountList;
//import com.conygre.training.demo.accounts.dao.AccountMongoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountService {
    
    @Autowired
    //private AccountMongoRepository accountList;
    private AccountList accountList;

    public int countAccounts(){
        return accountList.findAll().size();
    }

    public void insertAccount(Account account) {
        this.accountList.insert(account);
    }
}