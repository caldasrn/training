package com.conygre.spring;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TestStrings {
    private String s1, s2, s3;
    private Boolean compareResult;


    @Before
    public void staging(){
        s1 = "hello";
        s2 = "hello";
        s3 = new String("hello");
        compareResult = false;
    }
    
    @Test
    public void comparingStringsWithDoubleEquals() {
        
        //Act
        compareResult = s1 == s2;
        //Assert
        assertTrue("== is working", compareResult);
    }

    @Test
    public void comparingStringToNewStringWithDoubleEquals(){

        //Act
        compareResult = s1 == s3;
        //Assert
        assertFalse("== did not work.", compareResult);
    }
}