import com.conygre.test.pets.Person;
import com.conygre.test.pets.Pet;
import com.conygre.test.pets.PetConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PersonTest {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(PetConfig.class);

        Person personFromSpring = context.getBean(Person.class);

        Pet petFromSpring = personFromSpring.getPet();

        petFromSpring.feed();
    }
}
