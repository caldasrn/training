package com.conygre.test.pets;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@Configuration
@ComponentScan("com.conygre.test")
public class PetConfig {
        
    // @Bean
    // public Pet pet(){
    //     return new Dog();
    // }

    // @Bean
    // public Person person(@Autowired Pet pet){
    //     return new Person(pet);
    // }
}