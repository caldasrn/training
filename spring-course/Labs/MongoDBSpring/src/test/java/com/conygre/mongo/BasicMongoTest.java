package com.conygre.mongo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.conygre.spring.MongoJavaConfig;
import com.conygre.spring.entities.CompactDisc;
import com.mongodb.BasicDBObject;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MongoJavaConfig.class, loader = AnnotationConfigContextLoader.class)
public class BasicMongoTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void canInserSuccessfully(){
        CompactDisc cd1 = new CompactDisc("Warning", "Green Day", 10.50);
        CompactDisc cd2 = new CompactDisc("Immortal", "Michael Jackson", 10.50);
        CompactDisc cd3 = new CompactDisc("Bark at the Moon", "Ozzy Osgourne", 10.50);

        mongoTemplate.insert(cd1);
        mongoTemplate.insert(cd2);
        mongoTemplate.insert(cd3);

        List<CompactDisc> cds = mongoTemplate.findAll(CompactDisc.class);
        cds.forEach(cd -> System.out.println(cd.getTitle()));
        assertEquals(3, cds.size());
    }

    @After
    public void cleanUp(){
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")){
                mongoTemplate.getCollection(collectionName).remove(new BasicDBObject());
            }
        }
    }
}